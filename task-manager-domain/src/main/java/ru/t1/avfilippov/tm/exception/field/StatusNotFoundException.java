package ru.t1.avfilippov.tm.exception.field;

public final class StatusNotFoundException extends AbstractFieldException {

    public StatusNotFoundException() {
        super("Error! Status is not found...");
    }

}
