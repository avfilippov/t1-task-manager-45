package ru.t1.avfilippov.tm.api.service.model;

import ru.t1.avfilippov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
