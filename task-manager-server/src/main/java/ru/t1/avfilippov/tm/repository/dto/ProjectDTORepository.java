package ru.t1.avfilippov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

    @Override
    @NotNull
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        return add(userId, project);
    }

    @Override
    @NotNull
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final ProjectDTO project = new ProjectDTO(name);
        return add(userId, project);
    }

}
