package ru.t1.avfilippov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.repository.model.ISessionRepository;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.dto.ISessionDTOService;
import ru.t1.avfilippov.tm.api.service.model.ISessionService;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.model.Session;
import ru.t1.avfilippov.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, SessionRepository>
        implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}
