package ru.t1.avfilippov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.service.ISaltProvider;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.service.PropertyService;
import ru.t1.avfilippov.tm.util.HashUtil;

import java.util.Date;

@UtilityClass
public final class TestData {

    @NotNull
    public final static ProjectDTO USER_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static TaskDTO USER_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN_TASK1 = new TaskDTO();

    @NotNull
    public final static UserDTO USER1 = new UserDTO();

    @NotNull
    public final static UserDTO USER2 = new UserDTO();

    @NotNull
    public final static ProjectDTO NULL_PROJECT = null;

    @NotNull
    public final static TaskDTO NULL_TASK = null;

    @NotNull
    public final static SessionDTO SESSION = new SessionDTO();

    @NotNull
    public final static SessionDTO SESSION_NULL = null;

    @NotNull
    public static final String LOGIN = "login";

    @NotNull
    public static final String PASSWORD = "pass";

    static {
        @Nullable final ISaltProvider propertyService = new PropertyService();

        USER1.setLogin("TEST1");
        USER1.setFirstName("Test");
        USER1.setLastName("User");
        USER1.setRole(Role.USUAL);
        USER1.setPasswordHash(HashUtil.salt(propertyService, "password"));

        USER2.setLogin("TEST2");
        USER2.setFirstName("Test");
        USER2.setLastName("Admin");
        USER2.setRole(Role.ADMIN);
        USER2.setPasswordHash(HashUtil.salt(propertyService, "password"));

        USER_PROJECT1.setUserId(USER1.getId());
        USER_PROJECT1.setName("First");
        USER_PROJECT1.setStatus(Status.IN_PROGRESS);
        USER_PROJECT1.setDescription("project");

        USER_PROJECT2.setUserId(USER1.getId());
        USER_PROJECT2.setName("Second User");
        USER_PROJECT2.setStatus(Status.IN_PROGRESS);
        USER_PROJECT2.setDescription("project");

        ADMIN_PROJECT1.setUserId(USER2.getId());
        ADMIN_PROJECT1.setName("Second");
        ADMIN_PROJECT1.setStatus(Status.COMPLETED);
        ADMIN_PROJECT1.setDescription("project");

        USER_TASK1.setUserId(USER1.getId());
        USER_TASK1.setName("First");
        USER_TASK1.setStatus(Status.IN_PROGRESS);
        USER_TASK1.setDescription("task");

        ADMIN_TASK1.setUserId(USER2.getId());
        ADMIN_TASK1.setName("Second");
        ADMIN_TASK1.setStatus(Status.COMPLETED);
        ADMIN_TASK1.setDescription("task");

        SESSION.setDate(new Date());
        SESSION.setRole(Role.USUAL);
        SESSION.setUserId(USER1.getId());
    }

}
